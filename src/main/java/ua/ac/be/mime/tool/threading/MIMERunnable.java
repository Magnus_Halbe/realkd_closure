package ua.ac.be.mime.tool.threading;

public interface MIMERunnable extends Runnable {

	public void stop();
}
