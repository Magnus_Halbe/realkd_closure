/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.data.table;

import static de.unibonn.realkd.data.table.XarfParsing.CAPTION_PARAMETER_NAME;
import static de.unibonn.realkd.data.table.XarfParsing.COMMENT;
import static de.unibonn.realkd.data.table.XarfParsing.LABEL_RELATION;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.data.Populations;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attributegroups.AttributeGroup;

/**
 * @author Panagiotis Mandros
 * 
 * @author Michael Hedderich
 * 
 * @author Mario Boley
 * 
 * @since 0.4.0
 * 
 * @version 0.5.0
 *
 */
public class XarfImport {

	private static final Logger LOGGER = Logger.getLogger(XarfImport.class.getName());

	private String id;
	private String name;
	private String filename;
	private String description;
	private List<String> attributeDeclarations;
	private List<String> groupDeclarations;
	private List<String> data;
	private List<AttributesFromGroupMapper> groupMappers = ImmutableList.copyOf(AttributesFromGroupMapper.values());

	private XarfImport() {
		;
	}

	public XarfImport groupMappers(List<AttributesFromGroupMapper> groupMappers) {
		this.groupMappers = groupMappers;
		return this;
	}

	public XarfImport filename(String filename) {
		this.filename = filename;
		return this;
	}

	public DataTable get() {
		this.data = new ArrayList<>();
		this.attributeDeclarations = new ArrayList<>();
		this.groupDeclarations = new ArrayList<>();
		parseARFF();
		List<List<String>> dataParsed = csvTo2dTable(data);
		Population population = extractPopulation(attributeDeclarations, dataParsed);
		ArrayList<Attribute<?>> attributes = XarfParsing.attributes(attributeDeclarations, dataParsed);
		List<AttributeGroup> groups = createGroups(groupDeclarations, attributes);
		attributes.addAll(derivedAttributes(groups));
		return DataTables.table(id, name, description, population, attributes, groups);
	}

	private List<Attribute<?>> derivedAttributes(List<AttributeGroup> groups) {
		Function<? super AttributeGroup, ? extends Stream<? extends Attribute<?>>> groupToAttributes = group -> groupMappers
				.stream().flatMap(m -> m.apply(group).stream());
		List<Attribute<?>> result = groups.stream().flatMap(groupToAttributes).collect(Collectors.toList());
		LOGGER.info(() -> "Done creating derived attributes (" + result.size() + " derived attributes created)");
		return result;
	}

	private Population extractPopulation(List<String> attributeDeclarations, List<List<String>> parsedData) {
		Optional<Integer> nameAttributeIndex = XarfParsing.nameAttributeIndex(attributeDeclarations);
		Optional<List<String>> names = nameAttributeIndex.flatMap(i -> XarfParsing.column(i, parsedData));
		if (names.isPresent()) {
			return Populations.population("population_of_" + id, "Population of " + name,
					"This population has been created automatically by xarf import.", names.get());

		} else {
			return Populations.population("population_of_" + id, "Population of " + name,
					"This population has been created automatically by xarf import.", data.size());
		}
	}

	// takes the data section, and makes it a list of lists, i.e., a 2D table
	private static List<List<String>> csvTo2dTable(List<String> dataRows) {
		List<List<String>> dataParsed = new ArrayList<>();
		int numSamples = dataRows.size();
		for (int i = 0; i < numSamples; i++) {
			String[] dataRowParsed = dataRows.get(i).replaceAll("\"", "").replaceAll("\'", "").split(",");
			ArrayList<String> newDataRow = new ArrayList<>();
			for (int j = 0; j < dataRowParsed.length; j++) {
				newDataRow.add(dataRowParsed[j].trim());
			}
			dataParsed.add(newDataRow);
		}
		return dataParsed;
	}

	private static List<AttributeGroup> createGroups(List<String> groupDeclarations, List<Attribute<?>> attributes) {
		Map<Identifier, Attribute<?>> attributeMap = new HashMap<>();
		attributes.forEach(a -> attributeMap.put(a.id(), a));
		ArrayList<AttributeGroup> result = new ArrayList<>();
		for (String line : groupDeclarations) {
			Optional<AttributeGroup> group = XarfParsing.groupEntry(line)
					.flatMap(e -> XarfParsing.group(e, attributeMap));
			if (!group.isPresent()) {
				LOGGER.warning("Could not parse group from line: " + line);
				continue;
			}
			result.add(group.get());
		}
		return result;
	}

	public static XarfImport xarfImport() {
		return new XarfImport();
	}

	public static XarfImport xarfImport(String filename) {
		return new XarfImport().filename(filename);
	}

	// parses an arff file
	// it assumes 3 things to be in order
	// 1: a description section (optional)
	// 2: an attribute section
	// 3: the data section
	private void parseARFF() {
		if (filename == null) {
			throw new IllegalStateException("Filename not provided");
		}
		LOGGER.fine("Parsing ARFF file");
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			String line;

			// used to indicate the parser is in a random comment, i.e.
			// a comment after the description section
			boolean isRandomComment = false;

			// used to indicate the parser is in the data section
			boolean isDataSection = false;

			// it will contain the description of the dataset
			StringBuilder toBeDescription = new StringBuilder("");

			while ((line = br.readLine()) != null) {
				// if line is empty, continue
				// if line is random comment, i.e., comment after the
				// description section, continue
				// also continue when inputs or outputes are provided (KEEL
				// format)
				if (Strings.isNullOrEmpty(line) || (line.startsWith(COMMENT) && isRandomComment == true)
						|| line.startsWith("@inputs") || line.startsWith("@outputs")) {
					continue;
				}

				// parse the description if it exists
				// if it exists, it starts with % and stops at @relation
				if ((line.startsWith(COMMENT) && !line.toLowerCase().startsWith(LABEL_RELATION))) {
					toBeDescription.append(line.replaceFirst(COMMENT, ""));
					toBeDescription.append(System.lineSeparator());
				}
				// if line starts with @relation (case insensitive), take the
				// name and id
				else if (line.toLowerCase().startsWith(LABEL_RELATION)) {
					// create the description
					description = toBeDescription.toString();
					String[] tokens = XarfParsing.declarationTokens(line);
					Map<String, String> parameters = XarfParsing.parameterValues(tokens, 2);
					// anything that starts with % is a random comment from now
					// on
					isRandomComment = true;
					// take the name of the dataset
					id = tokens[1];
					name = parameters.containsKey(CAPTION_PARAMETER_NAME) ? parameters.get(CAPTION_PARAMETER_NAME) : id;
					// name = line.split("\\s+")[1];
					// id = name;
					// if attributes just started
				} else if (line.toLowerCase().startsWith(XarfParsing.LABEL_ATTRIBUTE)) {
					attributeDeclarations.add(line);
					// if data just started
				} else if (line.toLowerCase().startsWith(XarfParsing.LABEL_GROUP)) {
					groupDeclarations.add(line);
				} else if (line.toLowerCase().startsWith(XarfParsing.LABEL_DATA_SECTION) || isDataSection == true) {
					if (isDataSection == false) {
						isDataSection = true;
						continue;
					} else {
						data.add(line.replaceAll("\"", "").replaceAll("\'", ""));
					}
				} else {
					LOGGER.warning("Unknown line format (forgot '@data' declaration?); skipping: " + line);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		LOGGER.info("Done parsing ARFF file");
	}

}
