/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2017 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.data.table;

import static de.unibonn.realkd.data.table.XarfParsing.categoricAttributeDeclaration;
import static de.unibonn.realkd.data.table.XarfParsing.integerAttributeDeclaration;
import static de.unibonn.realkd.data.table.XarfParsing.metricAttributeDeclaration;
import static de.unibonn.realkd.data.table.XarfParsing.nameAttributeDeclaration;
import static de.unibonn.realkd.data.table.XarfParsing.parseColumn;
import static de.unibonn.realkd.data.table.attribute.Attributes.categoricalAttribute;
import static de.unibonn.realkd.data.table.attribute.Attributes.metricDoubleAttribute;
import static de.unibonn.realkd.data.table.attribute.Attributes.orderedCategoricAttribute;
import static de.unibonn.realkd.data.table.attribute.Attributes.ordinalAttribute;
import static de.unibonn.realkd.data.table.attributegroups.AttributeGroups.distributionGroup;
import static de.unibonn.realkd.data.table.attributegroups.AttributeGroups.orderedDistributionGroup;
import static java.lang.Boolean.parseBoolean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.Attributes;
import de.unibonn.realkd.data.table.attribute.FiniteOrder;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.data.table.attribute.OrdinalAttribute;
import de.unibonn.realkd.data.table.attributegroups.AttributeGroup;
import de.unibonn.realkd.data.table.attributegroups.AttributeGroups;
import de.unibonn.realkd.data.table.attributegroups.DefaultAttributeGroup;

/**
 * 
 * @author Panagiotis Mandros
 * @author Michael Hedderich
 * @author Mario Boley
 * 
 * @since 0.5.0
 * 
 * @version 0.5.0
 *
 */
public class XarfParsing {

	private static final Logger LOGGER = Logger.getLogger(XarfParsing.class.getName());

	static class GroupEntry {

		public final String name;

		public final String groupType;

		public final Collection<String> members;

		public GroupEntry(String name, String groupType, Collection<String> members) {
			this.name = name;
			this.groupType = groupType;
			this.members = members;
		}

	}

	public static final String DESCRIPTION_PARAMETER_NAME = "description";
	public static final String CAPTION_PARAMETER_NAME = "caption";
	public static final String LABEL_RELATION = "@relation";
	public static final String LABEL_DATA_SECTION = "@data";
	public static final String LABEL_GROUP = "@group";
	public static final String LABEL_ATTRIBUTE = "@attribute";
	public static final String COMMENT = "%";
	
	private static final String MISSING_VALUE = "?";
	private static final String LABEL_OF_NAME_ATTRIBUTE = "name";
	private static final String LABEL_OF_CATEGORIC_ATTRIBUTE = "categoric";
	private static final String LABEL_OF_INTEGER_ATTRIBUTE = "integer";
	private static final String LABEL_OF_NUMERIC_ATTRIBUTE = "numeric";
	private static final String LABEL_OF_REAL_ATTRIBUTE = "real";
	private static final String LABEL_OF_DATE_ATTRIBUTE = "date";

	
	private static final Function<String, Integer> AS_INTEGER = s -> {
		try {
			return Integer.parseInt(s);
		} catch (NumberFormatException e) {
			LOGGER.warning("Value for integer attribute could not be parsed as integer: " + s);
			try {
				double d = Double.parseDouble(s);
				if (d != (int) d) {
					throw new NumberFormatException(d + " not an integer value");
				}
				return (int) d;
			} catch (NumberFormatException e2) {
				LOGGER.warning(String.format("Could not parse '%s' as a number", s));
				throw e2;
			}
		}
	};

	private static final Function<String, String> AS_STRING = s -> s;

	private static final Function<String, Double> AS_DOUBLE = s -> Double.parseDouble(s);

	
	private XarfParsing() {
		; // not to be instantiated
	}
	
	static ArrayList<Attribute<?>> attributes(List<String> attributeDeclarations,
			List<List<String>> dataParsed) {
		LOGGER.fine("Creating attributes");
		ArrayList<Attribute<?>> attributes = new ArrayList<>();

		// for every attribute declaration line found in initial pass
		for (int i = 0; i < attributeDeclarations.size(); i++) {
			try {
				// split on whitespaces
				String line = attributeDeclarations.get(i);
				String[] tokens = XarfParsing.declarationTokens(line);
				Map<String, String> parameters = XarfParsing.parameterValues(tokens, 3);
				final Identifier identifier = Identifier.identifier(tokens[1]);
				final String caption = XarfParsing.valueOfOr(CAPTION_PARAMETER_NAME, parameters, tokens[1]);
				final String description = XarfParsing.valueOfOr(DESCRIPTION_PARAMETER_NAME, parameters, "");

				if (metricAttributeDeclaration(tokens)) {
					List<Double> values = parseColumn(dataParsed, i, AS_DOUBLE);
					attributes.add(metricDoubleAttribute(identifier, caption, description, values));
				} else if (integerAttributeDeclaration(tokens)) {
					List<Integer> values = parseColumn(dataParsed, i, AS_INTEGER);
					boolean categoric = parseBoolean(XarfParsing.valueOfOr("categorical", parameters, "false"));
					OrdinalAttribute<Integer> attribute = categoric
							? orderedCategoricAttribute(identifier, caption, description, values, Integer.class)
							: ordinalAttribute(identifier, caption, description, values, Integer.class);
					attributes.add(attribute);
				} else if (stringOrder(tokens[2]).isPresent()) {
					List<String> values = parseColumn(dataParsed, i, AS_STRING);
					attributes.add(Attributes.orderedCategoricAttribute(identifier, caption, description, values,
							stringOrder(tokens[2]).get(), String.class));
				} else if (categoricAttributeDeclaration(tokens)) {
					List<String> values = parseColumn(dataParsed, i, AS_STRING);
					attributes.add(categoricalAttribute(identifier, caption, description, values));
				} else if (dateAttributeDeclaration(tokens)) {
					SimpleDateFormat formatter = new SimpleDateFormat(tokens[3]);
					List<Date> values = parseColumn(dataParsed, i, s -> {
						try {
							return formatter.parse(s);
						} catch (ParseException e) {
							e.printStackTrace();
						}
						return null;
					});
					attributes.add(Attributes.dateAttribute(identifier, caption, description, values));
				} else if (nameAttributeDeclaration(tokens)) {
					; // skip name attributes
				} else {
					LOGGER.warning(String.format("Skipping attribute '%s' with unknown domain specifier '%s'",
							identifier, tokens[2]));
				}
			} catch (Exception exc) {
				LOGGER.severe("Error; skipping attribute for declaration: " + attributeDeclarations.get(i));
			}
		}
		LOGGER.info(() -> "Done creating attributes (" + attributes.size() + " attributes created)");
		return attributes;
	}
	
	private static Optional<Comparator<String>> stringOrder(String token) {
		Optional<? extends Collection<String>> collection = XarfParsing.parseCollection(token);
		return collection.filter(c -> c instanceof List).map(c -> new FiniteOrder((List<String>) c));
	}



	static String[] declarationTokens(String line) {
		String regex = "\"([^\"]*)\"|'([^']*)'|\\{([^}]*.?)\\}|\\{([^}]*.?)\\}|([\\w]+)=|([\\S&&[^=]]+)";
		List<String> results = new ArrayList<>();
		Matcher m = Pattern.compile(regex).matcher(line);
		while (m.find()) {
			if (m.group(1) != null) {
				results.add(m.group(1));
			} else if (m.group(2) != null) {
				results.add(m.group(2));
			} else if (m.group(5) != null) {
				results.add(m.group(5));
			} else {
				results.add(m.group());
			}
		}
		return results.toArray(new String[results.size()]);
		// return line.replaceAll("\"", "").replaceAll("\'", "").split("\\s+");
	}

	static Optional<Collection<String>> parseCollection(String token) {
		String strippedFromBrackets = token.substring(1, token.length() - 1);
		String[] contentTokens = strippedFromBrackets.replaceAll("\"", "").replaceAll("\'", "").split(",");
		if (token.startsWith("{") && token.endsWith("}")) {
			return Optional.of(ImmutableSet.copyOf(contentTokens));
		} else if (token.startsWith("[") && token.endsWith("]")) {
			return Optional.of(ImmutableList.copyOf(contentTokens));
		} else {
			return Optional.empty();
		}
	}

	static Map<String, String> parameterValues(String[] tokens, int startingIndex) {
		Map<String, String> result = new HashMap<>();
		for (int i = startingIndex; i + 1 < tokens.length; i = i + 2) {
			result.put(tokens[i], tokens[i + 1]);
		}
		return result;
	}

	public static Optional<GroupEntry> groupEntry(String line) {
		String[] tokens = declarationTokens(line);
		if (tokens.length < 4) {
			LOGGER.warning("Insufficient number of tokens; skipping group declaration: " + line);
			return Optional.empty();
		}
		Optional<Collection<String>> memberCollectionTokens = parseCollection(tokens[3]);
		if (!memberCollectionTokens.isPresent()) {
			LOGGER.warning("Skipping group declaration " + tokens[1] + "; could not parse members from " + tokens[3]);
			return Optional.empty();
		}
		return Optional.of(new GroupEntry(tokens[1], tokens[2], memberCollectionTokens.get()));
	}

	private enum AttributeGroupFactory {
		JOINT_MACRO_ATTRIBUTE("functional_group") {
			@Override
			public AttributeGroup group(GroupEntry entry, Map<Identifier, Attribute<?>> attributes) {
				return AttributeGroups.functionalGroup(entry.name, toAttributeList(entry.members, attributes));
			}

		},
		DISTRIBUTION("distribution") {
			@Override
			public AttributeGroup group(GroupEntry entry, Map<Identifier, Attribute<?>> attributes) {
				return distributionGroup(entry.name, toDoubleAttributeList(entry.members, attributes));
			}
		},
		ORDERED_DISTRIBUTION("ordered_distribution") {
			@Override
			public AttributeGroup group(GroupEntry entry, Map<Identifier, Attribute<?>> attributes) {
				return orderedDistributionGroup(entry.name, toDoubleAttributeList(entry.members, attributes));
			}
		},
		HIERARCHY("hierarchy") {
			@Override
			public AttributeGroup group(GroupEntry entry, Map<Identifier, Attribute<?>> attributes) {
				return new DefaultAttributeGroup(entry.name, toAttributeList(entry.members, attributes));
			}
		},
		// SEQUENCE("sequence") {
		//
		// /*
		// * within sequence record
		// */
		// private static final int ATTRIBUTE_INDEX = 0;
		//
		// /*
		// * within sequence record
		// */
		// private static final int ELEMENT_NAME_INDEX = 1;
		//
		// @Override
		// public AttributeGroup getAttributeGroup(List<String> content,
		// List<Attribute<?>> attributeList) {
		//
		// List<String> stringGroupRecords = Arrays
		// .asList(content.get(ATTRIBUTE_GROUP_CONTENT_INDEX).split(ATTRIBUTE_GROUP_2ND_FIELD_DELIMITER));
		//
		// List<String> stringGroupIndices = new
		// ArrayList<>(stringGroupRecords.size());
		// List<String> elementNames = new
		// ArrayList<>(stringGroupRecords.size());
		// for (String groupRecord : stringGroupRecords) {
		// String[] groupRecordElements =
		// groupRecord.split(ATTRIBUTE_GROUP_3RD_FIELD_DELIMITER);
		// stringGroupIndices.add(groupRecordElements[ATTRIBUTE_INDEX]);
		// elementNames.add(groupRecordElements[ELEMENT_NAME_INDEX]);
		// }
		//
		// return new
		// DefaultMetricSequenceWithAbsoluteDifferences(content.get(ATTRIBUTE_GROUP_NAME_INDEX),
		// toDoubleAttributeList(stringGroupIndices, attributeList),
		// elementNames);
		// }
		// },
		CATEGORY_TAG("category_tag") {
			@Override
			public AttributeGroup group(GroupEntry entry, Map<Identifier, Attribute<?>> attributes) {
				return new DefaultAttributeGroup(entry.name, toAttributeList(entry.members, attributes));
			}
		};

		private static List<Attribute<?>> toAttributeList(Collection<String> attributeIdCollection,
				Map<Identifier, Attribute<?>> attributes) {
			List<Attribute<?>> result = new ArrayList<>();
			for (String idString : attributeIdCollection) {
				Identifier id = Identifier.identifier(idString);
				if (!attributes.containsKey(id)) {
					LOGGER.warning("No attribute with identifier: " + id + "; skipping");
					continue;
				}
				result.add(attributes.get(id));
			}
			return result;
		}

		private static List<MetricAttribute> toDoubleAttributeList(Collection<String> attributeIdCollection,
				Map<Identifier, Attribute<?>> attributes) {
			List<MetricAttribute> result = new ArrayList<>();
			for (String idString : attributeIdCollection) {
				Identifier id = Identifier.identifier(idString);
				Attribute<?> attribute = attributes.get(id);
				if (!(attribute instanceof MetricAttribute)) {
					LOGGER.warning("No metric attribute with identifier: " + id + "; skipping");
					continue;
				}
				result.add((MetricAttribute) attributes.get(id));
			}
			return result;
		}

		public static Optional<AttributeGroupFactory> getFactoryMatchingXarfTypeString(String dbRepresentation) {
			for (AttributeGroupFactory factory : AttributeGroupFactory.values()) {
				if (factory.xarfGroupTypeString.equals(dbRepresentation)) {
					return Optional.of(factory);
				}
			}
			return Optional.empty();
		}

		private String xarfGroupTypeString;

		AttributeGroupFactory(String dbRepresentationString) {
			this.xarfGroupTypeString = dbRepresentationString;
		}

		public abstract AttributeGroup group(GroupEntry entry, Map<Identifier, Attribute<?>> attributes);

	}

	public static Optional<AttributeGroup> group(GroupEntry entry, Map<Identifier, Attribute<?>> attributeMap) {
		Optional<AttributeGroupFactory> factory = AttributeGroupFactory
				.getFactoryMatchingXarfTypeString(entry.groupType);
		if (!factory.isPresent()) {
			LOGGER.warning("Unknown group type '" + entry.groupType + "'");
		}
		return factory.map(f -> f.group(entry, attributeMap));
	}

	static Optional<Integer> nameAttributeIndex(List<String> attributeDeclarations) {
		for (int i = 0; i < attributeDeclarations.size(); i++) {
			String[] tokens = attributeDeclarations.get(i).split(" ");
			if (tokens.length >= 3 && tokens[2].equals(XarfParsing.LABEL_OF_NAME_ATTRIBUTE)) {
				return Optional.of(i);
			}
		}
		return Optional.empty();
	}

	static Optional<List<String>> column(int i, List<List<String>> table) {
		List<String> result = new ArrayList<>();
		for (List<String> row : table) {
			if (row.size() <= i) {
				return Optional.empty();
			}
			result.add(row.get(i));
		}
		return Optional.of(result);
	}

	static <T> List<T> parseColumn(List<List<String>> data, int i, Function<String, T> parseEntry) {
		List<T> values = new ArrayList<>();
		for (int j = 0; j < data.size(); j++) {
			String cellValue = null;
			try {
				cellValue = data.get(j).get(i);
				if (cellValue.equals(XarfParsing.MISSING_VALUE)) {
					values.add(null);
				} else {
					values.add(parseEntry.apply(data.get(j).get(i)));
				}
			} catch (Exception e) {
				LOGGER.warning(String.format(
						"Exception when parsing value '%s' data column %d row %d (setting to missing)\nMessage: %s",
						cellValue, i, j, e.getMessage()));
				values.add(null);
			}
		}
		return values;
	}

	public static boolean nameAttributeDeclaration(String[] tokens) {
		return tokens[2].toLowerCase().startsWith(LABEL_OF_NAME_ATTRIBUTE);
	}

	public static boolean categoricAttributeDeclaration(String[] tokens) {
		return tokens[2].toLowerCase().startsWith(LABEL_OF_CATEGORIC_ATTRIBUTE) || tokens[2].contains("{");
	}

	public static boolean integerAttributeDeclaration(String[] tokens) {
		return tokens[2].toLowerCase().startsWith(LABEL_OF_INTEGER_ATTRIBUTE);
	}
	
	public static boolean metricAttributeDeclaration(String[] tokens) {
		return tokens[2].toLowerCase().startsWith(LABEL_OF_NUMERIC_ATTRIBUTE)
				|| tokens[2].toLowerCase().startsWith(LABEL_OF_REAL_ATTRIBUTE);
	}
	
	public static boolean dateAttributeDeclaration(String[] tokens) {
		return tokens[2].toLowerCase().startsWith(LABEL_OF_DATE_ATTRIBUTE);
	}

	public static <K, V> V valueOfOr(K key, Map<K, V> parameters, V defaultValue) {
		final V caption = parameters.containsKey(key) ? parameters.get(key) : defaultValue;
		return caption;
	}

}
