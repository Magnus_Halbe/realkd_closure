/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2017 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.run;

import static de.unibonn.realkd.common.workspace.NamedStringValue.value;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.unibonn.realkd.common.JsonSerializable;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.common.workspace.Workspaces;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.propositions.PropositionalContextFromTableBuilder;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.XarfImport;

/**
 * @author Mario Boley
 *
 * @version 0.6.0
 * 
 * @since 0.6.0
 *
 */
@JsonInclude(Include.NON_EMPTY)
public class WorkspaceFromXarf implements WorkspaceSpecification {

	public static WorkspaceFromXarf workspaceFromXarf(Identifier id, String xarfFilePath) {
		return new WorkspaceFromXarf(id, xarfFilePath, new HashMap<>());
	}

	public static WorkspaceFromXarf workspaceFromXarf(Identifier id, String xarfFilePath, Map<Identifier, String> values) {
		return new WorkspaceFromXarf(id, xarfFilePath, new HashMap<>(values));
	}

	@JsonProperty("id")
	private final Identifier id;

	@JsonProperty("datafile")
	private final String dataPath;

	@JsonProperty("values")
	private final HashMap<Identifier, String> values;

	@JsonCreator
	private WorkspaceFromXarf(@JsonProperty("id") Identifier id, @JsonProperty("datafile") String path,
			@JsonProperty("values") HashMap<Identifier, String> values) {
		this.id = id;
		this.dataPath = path;
		this.values = (values != null) ? values : new HashMap<>();
	}

	@Override
	public Workspace build(ExecutionContext context) throws ValidationException {
		String datafilePath=context.pathToData.resolve(dataPath).toString();
		DataTable table = XarfImport.xarfImport(datafilePath).get();
		Path workspacePath = context.outputWorkspaces.resolve(id().toString());
		try {
			Files.createDirectories(workspacePath);
		} catch (IOException e) {
			throw new ValidationException("could not create workspace directory", "check system privileges");
		}
		Workspace workspace = Workspaces.workspace(workspacePath);
		PropositionalContext propContext = new PropositionalContextFromTableBuilder().build(table);
		workspace.addAll(propContext);
		values.entrySet().forEach(e -> workspace.add(value(e.getKey().toString(), e.getValue())));
		return workspace;
	}

	@Override
	public Identifier id() {
		return id;
	}

}
