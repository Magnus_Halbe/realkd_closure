/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2017 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.run;

import static de.unibonn.realkd.util.IO.addFileLogger;
import static de.unibonn.realkd.util.IO.createDirectoryOrTerminate;
import static de.unibonn.realkd.util.IO.switchLogfile;
import static de.unibonn.realkd.util.IO.writeOut;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.unibonn.realkd.algorithms.MiningAlgorithm;
import de.unibonn.realkd.algorithms.StoppableMiningAlgorithm;
import de.unibonn.realkd.common.JsonSerializable;
import de.unibonn.realkd.common.JsonSerialization;
import de.unibonn.realkd.common.base.Identifiable;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.NamedPatternCollection;
import de.unibonn.realkd.patterns.Pattern;

/**
 * @author Mario Boley
 * 
 * @since 0.6.0
 * 
 * @version 0.6.0
 *
 */
public class ProductWorkScheme implements JsonSerializable, Runnable, Identifiable {

	public static ProductWorkScheme simpleExperiment(Identifier id, WorkspaceSpecification[] workspaces,
			ComputationSpecification[] computations, Tracker[] trackers, int computationTimeLimit, String dataPath) {
		return new ProductWorkScheme(id, workspaces, computations, trackers, computationTimeLimit, dataPath);
	}

	private static final Logger LOGGER = Logger.getLogger(ProductWorkScheme.class.getName());

	@JsonProperty("id")
	private final Identifier id;

	@JsonProperty("dataPath")
	@JsonInclude(Include.NON_EMPTY)
	private final String dataPathName;
	
	@JsonProperty("computationTimeLimit")
	@JsonInclude(Include.NON_EMPTY)
	private final Integer computationTimeLimit;
	
	@JsonProperty("workspaces")
	private final WorkspaceSpecification[] inputs;

	@JsonProperty("computations")
	@JsonInclude(Include.NON_EMPTY)
	private final ComputationSpecification[] computations;

	@JsonProperty("trackers")
	@JsonInclude(Include.NON_EMPTY)
	private final Tracker[] trackers;

	private final ExecutorService executor = Executors.newSingleThreadExecutor();

	private final ExecutionContext context;

	@JsonCreator
	private ProductWorkScheme(@JsonProperty("id") Identifier id,
			@JsonProperty("workspaces") WorkspaceSpecification[] inputs,
			@JsonProperty("computations") ComputationSpecification[] computations,
			@JsonProperty("trackers") Tracker[] trackers,
			@JsonProperty("computationTimeLimit") Integer computationTimeLimit,
			@JsonProperty("dataPath") String dataPathName) {
		this.id = id;
		this.inputs = inputs;
		this.computations = computations != null ? computations : new ComputationSpecification[0];
		this.trackers = trackers != null ? trackers : new Tracker[0];
		this.computationTimeLimit = computationTimeLimit;
		this.dataPathName = (dataPathName != null) ? dataPathName : ".";

		Path dataPath = FileSystems.getDefault().getPath(dataPathName);
		Path outputRoot = FileSystems.getDefault().getPath(id.toString(), new Date().toString());
		Path workspacePath = outputRoot.resolve("workspaces");
		this.context = ExecutionContext.executionContext(outputRoot, workspacePath, dataPath);
	}

	@Override
	public Identifier id() {
		return id;
	}

	private boolean timeOut(long startMillis) {
		if (computationTimeLimit == null) {
			return false;
		}
		return (System.currentTimeMillis() - startMillis) / 1000 > computationTimeLimit;
	}

	@Override
	public void run() {
		createDirectoryOrTerminate(context.outputRoot);
		writeOut(context.outputRoot.resolve("schemeBackup.json"), JsonSerialization.toPrettyJson(this));
		Path logPath = context.outputRoot.resolve("logs");
		createDirectoryOrTerminate(logPath);
		String generalLogfileName = logPath.resolve("general.log").toString();
		addFileLogger("", generalLogfileName);
		for (WorkspaceSpecification input : inputs) {
			try {
				Workspace workspace = input.build(context);
				for (ComputationSpecification computationBuilder : computations) {
					String computationLogfile = logPath.resolve(computationBuilder.id() + "_" + input.id() + ".log")
							.toString();
					switchLogfile("", generalLogfileName, computationLogfile);					
					try {
						MiningAlgorithm computation = computationBuilder.build(workspace);
						Future<Collection<? extends Pattern<?>>> futureResult = executor.submit(computation);
						long startTime = System.currentTimeMillis();
						while (!futureResult.isDone()) {
							if (timeOut(startTime) && computation instanceof StoppableMiningAlgorithm) {
								LOGGER.info("Sending stop signal to algorithm");
								((StoppableMiningAlgorithm) computation).requestStop();
								synchronized (this) {
									wait(1000);
								}
								if (!futureResult.isDone()) {
									LOGGER.info("Trying to cancel execution on thread level");
									futureResult.cancel(true);
								}
							}
							synchronized (this) {
								wait(1000);
							}
						}
						switchLogfile("", computationLogfile, generalLogfileName);
						Collection<? extends Pattern<?>> result = futureResult.get();
						for (Tracker tracker : trackers)
							tracker.consume(input.id(), computationBuilder.id(), computation, result);
						workspace.add(new NamedPatternCollection("$results_of_" + computationBuilder.id().toString(),
								"", "", result));
					} catch (ValidationException e) {
						LOGGER.warning(e.getMessage());
						LOGGER.warning(e.hint());
						continue;
					}
					catch (InterruptedException e) {
						e.printStackTrace();
					} catch (ExecutionException e) {
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				LOGGER.severe(e.getMessage() + "; skipping input");
				e.printStackTrace();
			}
		}
		executor.shutdownNow();
		Path metricsPath = context.outputRoot.resolve("metrics");
		createDirectoryOrTerminate(metricsPath);
		for (Tracker tracker : trackers)
			tracker.writeResults(metricsPath);
	}

}
