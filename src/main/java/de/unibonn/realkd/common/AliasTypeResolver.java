/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2017 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.common;

import java.io.IOException;
import java.util.logging.Logger;

import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.databind.DatabindContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;
import com.fasterxml.jackson.databind.jsontype.impl.ClassNameIdResolver;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import de.unibonn.realkd.data.constraints.Constraints;
import de.unibonn.realkd.data.constraints.EqualsConstraint;
import de.unibonn.realkd.data.constraints.GreaterOrEqualsConstraints;
import de.unibonn.realkd.data.constraints.GreaterThanConstraint;
import de.unibonn.realkd.data.constraints.LessOrEqualsConstraint;
import de.unibonn.realkd.data.constraints.LessThanConstraint;
import de.unibonn.realkd.data.table.attribute.FiniteOrder;
import de.unibonn.realkd.patterns.NamedPatternCollection;
import de.unibonn.realkd.patterns.association.Associations.AssociationBuilderImplementation;
import de.unibonn.realkd.patterns.emm.ExceptionalModelMining;
import de.unibonn.realkd.patterns.emm.NormalizedPositiveMedianShift;
import de.unibonn.realkd.patterns.logical.LogicalDescriptors;
import de.unibonn.realkd.patterns.models.gaussian.UnivariateGaussian;
import de.unibonn.realkd.patterns.models.mean.MetricEmpiricalDistribution;
import de.unibonn.realkd.patterns.models.regression.LinearRegressionModel;
import de.unibonn.realkd.patterns.pmm.PureModelMining.PureModelPatternSerialForm;
import de.unibonn.realkd.patterns.subgroups.Subgroups;
import de.unibonn.realkd.run.ComputationMeasureTracker;
import de.unibonn.realkd.run.LegacyComputationSpecification;
import de.unibonn.realkd.run.ProductWorkScheme;
import de.unibonn.realkd.run.ResultMeasureTracker;
import de.unibonn.realkd.run.WorkspaceFromXarf;

/**
 * Resolves types for json serialization with Jackson based on internal map of
 * aliases for java class names. In case no alias is registered for a type it
 * resolves class names based on fully qualified name.
 * 
 * @author Mario Boley
 * 
 * @since 0.6.0
 * 
 * @version 0.6.0
 *
 */
public final class AliasTypeResolver implements TypeIdResolver {

	private static final Logger LOGGER = Logger.getLogger(AliasTypeResolver.class.getName());

	private final static BiMap<String, String> ID_TO_CLASSNAME = HashBiMap.create();
	static {
		ID_TO_CLASSNAME.put("association", AssociationBuilderImplementation.class.getName());
		ID_TO_CLASSNAME.put("attributeBasedConjunction",
				LogicalDescriptors.AttributeBasedLogicalDescriptorSerialForm.class.getName());
		ID_TO_CLASSNAME.put("computationMeasureTracker", ComputationMeasureTracker.class.getName());
		ID_TO_CLASSNAME.put("equals", EqualsConstraint.class.getName());
		ID_TO_CLASSNAME.put("exceptionalSubgroupPattern",
				ExceptionalModelMining.ExceptionalModelPatternSerialForm.class.getName());
		ID_TO_CLASSNAME.put("finiteOrder", FiniteOrder.class.getName());
		ID_TO_CLASSNAME.put("greaterOrEquals", GreaterOrEqualsConstraints.class.getName());
		ID_TO_CLASSNAME.put("greaterThan", GreaterThanConstraint.class.getName());
		ID_TO_CLASSNAME.put("legacyComputation", LegacyComputationSpecification.class.getName());
		ID_TO_CLASSNAME.put("lessOrEquals", LessOrEqualsConstraint.class.getName());
		ID_TO_CLASSNAME.put("lessThan", LessThanConstraint.class.getName());
		ID_TO_CLASSNAME.put("metricEmpiricalDistribution",
				MetricEmpiricalDistribution.MetricEmpiricalDistributionExportableForm.class.getName());
		ID_TO_CLASSNAME.put("posNormMedianShift", NormalizedPositiveMedianShift.class.getName());
		ID_TO_CLASSNAME.put("namedConstraint", Constraints.NamedConstraint.class.getName());
		ID_TO_CLASSNAME.put("patternCollection",
				NamedPatternCollection.NamedPatternCollectionSerialForm.class.getName());
		ID_TO_CLASSNAME.put("productWorkScheme", ProductWorkScheme.class.getName());
		ID_TO_CLASSNAME.put("pureSubgroupPattern", PureModelPatternSerialForm.class.getName());
		ID_TO_CLASSNAME.put("resultMeasureTracker", ResultMeasureTracker.class.getName());
		ID_TO_CLASSNAME.put("subgroup", Subgroups.SubgroupSerialForm.class.getName());
		ID_TO_CLASSNAME.put("univariateGaussian", UnivariateGaussian.class.getName());
		ID_TO_CLASSNAME.put("univariateLinearRegressionModel", LinearRegressionModel.class.getName());
		ID_TO_CLASSNAME.put("workspaceFromXarf", WorkspaceFromXarf.class.getName());
	}

	private final ClassNameIdResolver classBasedResolver = new ClassNameIdResolver(TypeFactory.unknownType(),
			TypeFactory.defaultInstance());

	@Override
	public String getDescForKnownTypeIds() {
		return "full class names plus: " + ID_TO_CLASSNAME.keySet();
	}

	@Override
	public Id getMechanism() {
		return Id.CUSTOM;
	}

	@Override
	public String idFromBaseType() {
		return classBasedResolver.idFromBaseType();
	}

	@Override
	public String idFromValue(Object value) {
		String id = ID_TO_CLASSNAME.inverse().get(value.getClass().getName());
		if (id != null) {
			return id;
		}
		return classBasedResolver.idFromValue(value);
	}

	@Override
	public String idFromValueAndType(Object value, Class<?> clazz) {
		String id = ID_TO_CLASSNAME.inverse().get(value.getClass().getName());
		if (id != null) {
			return id;
		}
		return classBasedResolver.idFromValueAndType(value, clazz);
	}

	@Override
	public void init(JavaType clazz) {
		classBasedResolver.init(clazz);
	}

	@Override
	public JavaType typeFromId(DatabindContext context, String id) throws IOException {
		String className = ID_TO_CLASSNAME.get(id);
		if (className != null) {
			try {
				return TypeFactory.defaultInstance().constructSpecializedType(TypeFactory.unknownType(),
						Class.forName(className));
			} catch (ClassNotFoundException e) {
				LOGGER.severe(String.format("Could not find class '%1$s' for id '%2$s'", className, id));
			}
		}
		LOGGER.info(String.format("Could not find alias id '%1$s'; defaulting to class based resolver", id));
		return classBasedResolver.typeFromId(context, id);
	}

}