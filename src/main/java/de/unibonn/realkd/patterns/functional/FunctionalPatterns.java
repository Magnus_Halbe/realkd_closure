/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.functional;

import static de.unibonn.realkd.patterns.functional.ReliableFractionOfInformation.RELIABLE_FRACTION_OF_INFORMATION;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;

import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.Population;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.DefaultPattern;
import de.unibonn.realkd.patterns.models.table.ContingencyTable;
import de.unibonn.realkd.patterns.models.table.ContingencyTables;

/**
 * @author Mario Boley
 * @author Panagiotis Mandros
 * 
 * @since 0.3.1
 * 
 * @version 0.5.0
 *
 */
public class FunctionalPatterns {

	public static CorrelationDescriptor correlationDescriptor(DataTable table, Set<Attribute<?>> domain,
			Set<Attribute<?>> coDomain) {
		List<Attribute<?>> attributes = new ArrayList<>();
		attributes.addAll(domain);
		attributes.addAll(coDomain);
		ContingencyTable contingencyTable = ContingencyTables.contingencyTable(table, attributes);
		return new CorrelationDescriptorImplementation(table, domain, coDomain, contingencyTable);
	}

	public static FunctionalPattern functionalPattern(CorrelationDescriptor descriptor) {
		return functionalPattern(descriptor, RELIABLE_FRACTION_OF_INFORMATION);
	}

	public static FunctionalPattern functionalPattern(CorrelationDescriptor descriptor,
			FunctionalDependencyMeasure measure, Measurement... additionalMeasurements) {
		List<Measurement> measurements = new ArrayList<>();
		measurements.add(measure.perform(descriptor));
		stream(additionalMeasurements).forEach(m -> measurements.add(m));
		return new FunctionalPatternImplementation(descriptor.table().population(), descriptor, measurements);
	}

	public static FunctionalPattern functionalPattern(CorrelationDescriptor descriptor,
			Measurement dependencyMeasurement, Measurement... additionalMeasurements) {
		List<Measurement> measurements = new ArrayList<>();
		measurements.add(dependencyMeasurement);
		stream(additionalMeasurements).forEach(m -> measurements.add(m));
		return new FunctionalPatternImplementation(descriptor.table().population(), descriptor, measurements);
	}

	private static class CorrelationDescriptorImplementation implements CorrelationDescriptor {

		private final DataTable table;

		private final Set<Attribute<?>> domain;

		private final Set<Attribute<?>> coDomain;

		private final List<Attribute<?>> allReferenced;

		private final ContingencyTable contingencyTable;

		private CorrelationDescriptorImplementation(DataTable table, Set<Attribute<?>> domain,
				Set<Attribute<?>> coDomain, ContingencyTable contingencyTable) {
			this.domain = domain;
			this.coDomain = coDomain;
			this.allReferenced = ImmutableList.copyOf(Sets.union(domain, coDomain));
			this.table = table;
			this.contingencyTable = contingencyTable;
		}

		@Override
		public List<Attribute<?>> getReferencedAttributes() {
			return allReferenced;
		}

		@Override
		public Set<Attribute<?>> domain() {
			return domain;
		}

		@Override
		public Set<Attribute<?>> coDomain() {
			return coDomain;
		}

		@Override
		public SerialForm<CorrelationDescriptor> serialForm() {
			return new CorrelationDescriptorSerialFormImplementation(table.identifier(),
					domain.stream().map(a -> a.id()).collect(toList()),
					coDomain.stream().map(a -> a.id()).collect(toList()));
		}

		@Override
		public DataTable table() {
			return table;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (!(o instanceof CorrelationDescriptor)) {
				return false;
			}
			CorrelationDescriptor that = (CorrelationDescriptor) o;
			return this.domain().equals(that.domain()) && this.coDomain().equals(that.coDomain());
		}

		@Override
		public int hashCode() {
			return Objects.hash(domain, coDomain);
		}

		@Override
		public String toString() {
			return "(" + domain.toString() + "," + coDomain.toString() + ")";
		}

		@Override
		public ContingencyTable contingencyTable() {
			return contingencyTable;
		}
	}

	private static class CorrelationDescriptorSerialFormImplementation implements SerialForm<CorrelationDescriptor> {

		@JsonProperty("tableId")
		private final String tableId;

		@JsonProperty("domain")
		private final List<Identifier> domain;

		@JsonProperty("coDomain")
		private final List<Identifier> coDomain;

		private final ImmutableList<String> dependencies;

		@JsonCreator
		public CorrelationDescriptorSerialFormImplementation(@JsonProperty("tableId") String tableId,
				@JsonProperty("domain") List<Identifier> domain, @JsonProperty("coDomain") List<Identifier> coDomain) {
			this.tableId = tableId;
			this.domain = domain;
			this.coDomain = coDomain;
			this.dependencies = ImmutableList.of(tableId);
		}

		@Override
		public CorrelationDescriptor build(Workspace workspace) {
			DataTable table = workspace.get(tableId, DataTable.class).get();
			Function<Identifier, Attribute<?>> idToAttribute = id -> table.attribute(id).get();
			return correlationDescriptor(table, domain.stream().map(idToAttribute).collect(toSet()),
					coDomain.stream().map(idToAttribute).collect(toSet()));
		}

		public List<String> dependencyIds() {
			return dependencies;
		}

	}

	private static class FunctionalPatternImplementation extends DefaultPattern<CorrelationDescriptor>
			implements FunctionalPattern {

		private final FunctionalDependencyMeasure measure;

		/**
		 * 
		 * @param population
		 * @param descriptor
		 * @param measurements
		 *            non-empty list of measurements computed for this correlation
		 *            descriptor, the first of which has to be of a
		 *            {@link FunctionalDependencyMeasure}
		 */
		public FunctionalPatternImplementation(Population population, CorrelationDescriptor descriptor,
				List<Measurement> measurements) {
			super(population, descriptor, measurements);
			measure = (FunctionalDependencyMeasure) measurements.get(0).measure();
		}

		@Override
		public CorrelationDescriptor descriptor() {
			return (CorrelationDescriptor) super.descriptor();
		}

		@Override
		public FunctionalDependencyMeasure functionalityMeasure() {
			return measure;
		}

		@Override
		public SerialForm<FunctionalPattern> serialForm() {
			return new FunctionPatternSerialFormImplementation(descriptor().serialForm(), measurements());
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (!(o instanceof FunctionalPattern)) {
				return false;
			}

			FunctionalPattern that = (FunctionalPattern) o;
			return this.descriptor().domain().equals(that.descriptor().domain())
					&& this.descriptor().coDomain().equals(that.descriptor().coDomain());
		}

		@Override
		public int hashCode() {
			return Objects.hash(this.descriptor().domain(), this.descriptor().coDomain());
		}

	}

	private static class FunctionPatternSerialFormImplementation implements SerialForm<FunctionalPattern> {

		@JsonProperty("descriptor")
		private final SerialForm<CorrelationDescriptor> descriptor;

		@JsonProperty("measurements")
		private final List<Measurement> measurements;

		@JsonCreator
		public FunctionPatternSerialFormImplementation(
				@JsonProperty("descriptor") SerialForm<CorrelationDescriptor> descriptor,
				@JsonProperty("measurements") List<Measurement> measurements) {
			this.descriptor = descriptor;
			this.measurements = measurements;
		}

		@Override
		public FunctionalPattern build(Workspace workspace) {
			CorrelationDescriptor attributeRelation = descriptor.build(workspace);
			return new FunctionalPatternImplementation(attributeRelation.table().population(), attributeRelation,
					measurements);
		}

		@Override
		public Collection<String> dependencyIds() {
			return descriptor.dependencyIds();
		}

	}

}
