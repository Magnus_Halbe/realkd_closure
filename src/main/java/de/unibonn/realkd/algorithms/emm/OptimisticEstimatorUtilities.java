/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2017 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.algorithms.emm;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntToDoubleFunction;
import java.util.function.IntUnaryOperator;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.common.collect.Lists;

import de.unibonn.realkd.algorithms.branchbound.BranchAndBoundSearch.BranchAndBoundSearchNode;
import de.unibonn.realkd.common.IndexSet;
import de.unibonn.realkd.data.table.attribute.CategoricAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;

/**
 * Utility functions serving as an abstraction layer from realKD internals.
 * @author Janis Kalofolias
 *
 */
public class OptimisticEstimatorUtilities {
	/**
	 * Create the realKD-internals-agnostic PopulationData class from the taget and control variables.
	 * The PopulationData class tracks the non-missing target and control value pairs, along with their initial
	 * indices, to facilitate efficient selecting of a sorted subset of the population at the B&B Search Nodes.  
	 * @param target The target attribute. Must be continuous.
	 * @param control The control attribute. Must be discrete.
	 * @return A PopulationData structure
	 * @see PopulationData
	 */
	public static <T> PopulationData makePopulationData(MetricAttribute target, CategoricAttribute<T> control) {
		// Reference the data from the attributes
		List<Optional<Double>> lstTargFull = target.getValues();
		List<Optional<T>> lstCtrlFull = control.getValues();

		// Get sorted non-missing targets and controls, in ascending order
		List<Integer> idxAsc = target.sortedNonMissingRowIndices();
		int[] idxOrder = Lists.reverse(idxAsc).stream().filter(i -> lstCtrlFull.get(i).isPresent())
				.mapToInt(Integer::intValue).toArray();

		IntToDoubleFunction fTarget = i -> lstTargFull.get(i).get();
		IntStream sOrderPop = IntStream.of(idxOrder);
		IntUnaryOperator fControl;
		{ // Map control values to indices
			Map<Object, Integer> mapC2U;
			AtomicInteger idxAt = new AtomicInteger(0);
			mapC2U = control.categories().stream().collect(Collectors.toMap(k -> k, k -> idxAt.getAndIncrement()));
			fControl = i -> {
				Optional<T> ctrlVal = lstCtrlFull.get(i);
				return ctrlVal.isPresent() ? mapC2U.get(ctrlVal.get()) : 0;
			};
		}
		int numFull = target.getValues().size();
		int numCat = control.categories().size();
		return new PopulationData(fTarget, fControl, sOrderPop, numFull, numCat);
	}
}

/**
 * Optimistic estimator adaptor hiding the realKD-internals.
 * @author Janis Kalofolias
 * 
 */
class OptimisticEstimatorAdaptor 
	implements ToDoubleFunction<BranchAndBoundSearchNode<ExceptionalModelPattern>> {
	
	/** Population data instance for the entire population */ 
	private final PopulationData dataPop;
	/** Optimistic estimator that maps the SelectionData to a double. */
	private final ToDoubleFunction<SelectionData> fnOE;
	
	/**
	 * Create an optimistic estimator adaptor.
	 * @param dataPop a data population class corresponding to the population.
	 * @param fnOE  implementation that maps a 
	 */
	public OptimisticEstimatorAdaptor(PopulationData dataPop, ToDoubleFunction<SelectionData> fnOE) {
		this.dataPop = dataPop;
		this.fnOE = fnOE;
	}
	
	@Override
	public double applyAsDouble(BranchAndBoundSearchNode<ExceptionalModelPattern> node) {
		final IndexSet setQ = node.content.descriptor().supportSet();
		final int numSub = setQ.size();
		final SelectionData dataSel = new SelectionData(dataPop, setQ::contains, numSub);
		final double fVal = fnOE.applyAsDouble(dataSel);
		return fVal;
	}
}